# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170405061148) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "dictionaries", force: :cascade do |t|
    t.string   "dict_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.index ["user_id"], name: "index_dictionaries_on_user_id", using: :btree
  end

  create_table "dictionaries_trainers", id: false, force: :cascade do |t|
    t.integer "trainer_id",    null: false
    t.integer "dictionary_id", null: false
    t.index ["dictionary_id"], name: "index_dictionaries_trainers_on_dictionary_id", using: :btree
    t.index ["trainer_id"], name: "index_dictionaries_trainers_on_trainer_id", using: :btree
  end

  create_table "groups", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "level_id"
    t.index ["level_id"], name: "index_groups_on_level_id", using: :btree
  end

  create_table "levels", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "value"
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
    t.index ["name"], name: "index_roles_on_name", using: :btree
  end

  create_table "trainers", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_trainers_on_user_id", using: :btree
  end

  create_table "trainers_words", id: false, force: :cascade do |t|
    t.integer "trainer_id", null: false
    t.integer "word_id",    null: false
    t.integer "status"
    t.string  "answer"
    t.index ["trainer_id"], name: "index_trainers_words_on_trainer_id", using: :btree
    t.index ["word_id"], name: "index_trainers_words_on_word_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "group_id"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["group_id"], name: "index_users_on_group_id", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree
  end

  create_table "words", force: :cascade do |t|
    t.string   "foreign_word"
    t.string   "translate"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "dictionary_id"
    t.index ["dictionary_id"], name: "index_words_on_dictionary_id", using: :btree
  end

  add_foreign_key "dictionaries", "users"
  add_foreign_key "groups", "levels"
  add_foreign_key "trainers", "users"
  add_foreign_key "users", "groups"
  add_foreign_key "words", "dictionaries"
end
