class CreateWords < ActiveRecord::Migration[5.0]
  def change
    create_table :words do |t|
      t.string :foreign_word
      t.string :translate
      t.timestamps
    end
  end
end