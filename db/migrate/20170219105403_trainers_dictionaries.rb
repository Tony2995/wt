class TrainersDictionaries < ActiveRecord::Migration[5.0]

    def change
      create_join_table :trainers, :dictionaries do |t|
        t.index :trainer_id
        t.index :dictionary_id
      end
    end


end
