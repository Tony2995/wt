class AddGroupIdToUser < ActiveRecord::Migration[5.0]
  def change
    add_belongs_to :users, :group, foreign_key: true
  end
end
