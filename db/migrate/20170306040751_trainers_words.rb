class TrainersWords < ActiveRecord::Migration[5.0]
  def change
    create_join_table :trainers, :words do |t|
      t.index :trainer_id
      t.index :word_id
      t.integer :status
      t.string :answer
    end

  end
end

