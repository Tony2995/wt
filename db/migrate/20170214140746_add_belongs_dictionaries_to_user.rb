class AddBelongsDictionariesToUser < ActiveRecord::Migration[5.0]
  def change
    add_belongs_to :dictionaries, :user, foreign_key: true
  end
end
