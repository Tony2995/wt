class AddLevelIdToGroups < ActiveRecord::Migration[5.0]
  def change
    add_belongs_to :groups, :level, foreign_key: true
  end
end
