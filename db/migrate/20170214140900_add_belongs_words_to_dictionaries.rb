class AddBelongsWordsToDictionaries < ActiveRecord::Migration[5.0]
  def change
    add_belongs_to :words, :dictionary, foreign_key: true
  end
end
