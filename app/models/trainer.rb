class Trainer < ApplicationRecord
  belongs_to :user
  has_and_belongs_to_many :dictionaries
  has_and_belongs_to_many :words
end
