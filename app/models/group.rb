class Group < ApplicationRecord
  belongs_to :level, optional: true
  has_many :users
end
