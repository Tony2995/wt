class Word < ApplicationRecord
  belongs_to :dictionary
  has_and_belongs_to_many :trainers
end
