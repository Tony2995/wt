class Dictionary < ApplicationRecord
  has_many :words
  belongs_to :user
  has_and_belongs_to_many :trainers
  validates :dict_name, presence: true
end
