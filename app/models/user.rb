class User < ApplicationRecord
  rolify
  after_create :set_default_role
  has_many :dictionaries
  has_many :words_trainers
  belongs_to :group, optional: true

  def set_default_role
    self.add_role(:guest) if self.roles.blank?
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable
end
