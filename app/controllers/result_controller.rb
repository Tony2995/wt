class ResultController < ApplicationController
  layout 'result'

  def index
    @true_answers = session[:testing]['answers'].size

    @errors = session[:testing]['errors']
  end
end
