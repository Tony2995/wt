class TestingController < ApplicationController
  layout 'testing'

  def index
    # взять все слова для теста из сессии
    @words_for_testing = session[:testing]['words']
    #  в цикле брать одно слово
    for index in @words_for_testing
      index = session[:testing]['progress'].to_i
      session[:testing]['current_hash'] = @words_for_testing[index]

      redirect_to index_result_url and return  unless @words_for_testing.include? session[:testing]['current_hash']
      @word = session[:testing]['current_hash']['foreign_word']
    end
  end

  def answer
    answer = answer_params[:answer]
    session[:testing]['progress'] += 1

    if answer == session[:testing]['current_hash']['translate']
      session[:testing]['answers'] << session[:testing]['current_hash']
    else
      session[:testing]['errors'] << session[:testing]['current_hash']
    end

    redirect_to testing_index_url
  end

  private

  def answer_params
    params.require(:answer).permit(:answer)
  end
end
