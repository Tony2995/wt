class Teacher::TeacherController < ApplicationController
  layout 'teacher'
  def index
    @user = current_user
    authorize User
  end
end
