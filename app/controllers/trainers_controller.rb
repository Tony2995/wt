class TrainersController < ApplicationController

  def index
    @trainer = Trainer.new
    @dictionaries = current_user.dictionaries
  end

  def start

    @trainer = Trainer.new(words_testing_params)
    @trainer.user = current_user

    if @trainer.save

      @dictionary_for_testing = @trainer.dictionaries


      @words_for_training = Word.where(dictionary_id: @dictionary_for_testing)
      @trainer.words << @words_for_training

      @words = []

      @words_for_training.each do |word|

        @words << {foreign_word: word.foreign_word, translate: word.translate}

      end

      session[:testing] = {
          words: @words,
          progress: 0,
          answers: [],
          errors: [],
          current_hash: {}
      }


      redirect_to testing_index_url

    end

  end


  private

  def words_testing_params
    params.fetch(:trainer, {}).permit(dictionary_ids: [])
  end

end
