class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery with: :exception
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  before_action :authenticate_user!

  before_action :get_users_dictionaries


  private

  def user_not_authorized
    flash[:alert] = "Access denied"
    redirect_to(request.referrer || root_path)
  end

  def get_users_dictionaries
    if user_signed_in?
      @dictionaries = current_user.dictionaries
    end
  end
end
