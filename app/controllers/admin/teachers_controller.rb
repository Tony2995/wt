class Admin::TeachersController < Admin::AdminController
  def index
    @teachers = User.with_role :teacher
    authorize User
  end
end
