class Admin::LevelsController < Admin::AdminController
  def index
    @levels = Level.all
    @level = Level.new
  end

  def new
    @level = Level.new
  end

  def create
    @level = Level.new(level_params)
    @level.save
    redirect_to admin_levels_path
  end

  private

  def level_params
    params.require(:level).permit(:value)
  end
end
