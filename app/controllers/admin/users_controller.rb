class Admin::UsersController < Admin::AdminController
  def index
    @users = User.with_role :student
    @groups = Group.all
  end

  def join_to_group
    user = User.find_by(id: join_params[:user_id])
    user.update(group_id: join_params[:group_id])
    redirect_to admin_users_path
  end

  private

  def join_params
    params.permit(:group_id, :user_id)
  end
end
