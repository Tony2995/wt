class Admin::GroupsController < Admin::AdminController
  def index
    @groups = Group.order(:name)
    @group = Group.new
    @levels = Level.all
  end

  def create
    @group = Group.new(group_params)
    if @group.save
      flash[:alert] = "Группа \"#{@group.name}\" успешно создана"
      redirect_to admin_groups_path
    end
  end

  def join_to_level
    @group = Group.find(join_params[:group_id])
    @group.update_attribute(:level_id, join_params[:level_id])
    redirect_to admin_groups_path
  end

  private

  def group_params
    params.require(:group).permit(:name, :level_id)
  end

  def join_params
    params.permit(:level_id, :group_id)
  end
end
