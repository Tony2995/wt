class Admin::GuestsController < Admin::AdminController
  def index
    @guests = User.with_role :guest
    @groups = Group.all
  end

  def guest_to_group
    user = User.find_by(id: join_params[:guest_id])
    user.update(group_id: join_params[:group_id])
    user.remove_role :guest
    user.add_role :student
    redirect_to admin_users_path
  end

  def join_params
    params.permit(:group_id, :guest_id)
  end
end
