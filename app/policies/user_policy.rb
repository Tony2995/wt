class UserPolicy < ApplicationPolicy

def index?
  @user.has_role?(:admin) || @user.has_role?(:teacher)
end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
