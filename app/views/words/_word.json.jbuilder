json.extract! word, :id, :foreign_word, :translate, :created_at, :updated_at
json.url word_url(word, format: :json)