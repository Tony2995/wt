json.extract! dictionary, :id, :dict_name, :created_at, :updated_at
json.url dictionary_url(dictionary, format: :json)