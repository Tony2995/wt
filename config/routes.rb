Rails.application.routes.draw do
  devise_for :users

  resources :dictionaries, shallow: true do
    resources :words
  end

  namespace :admin do
    resources :users, only: [:index]
    resources :levels, only: [:index, :create]
    resources :teachers
    resources :guests, only: [:index]
    post 'guest_to_group' => 'guests#guest_to_group'
    post 'join_to_level' => 'groups#join_to_level', :as => :join_to_level
    post 'join_to_group' => 'users#join_to_group', :as => :join_to_group
    resources :groups, only: [:index, :create]
    resources :dictionaries
    get '/' => 'admin#index', :as => :root
  end

  namespace :teacher do
    get '/' => 'teacher#index', :as => :root
    resources :dictionaries
    resources :groups, only: [:index]
  end

  resources :testing, only: [:index]

  get 'result' => 'result#index', :as => :index_result

  post 'testing/answer' => 'testing#answer', :as => :answer_testing

  post 'trainers/start' => 'trainers#start', :as => :start_trainers

  resources :trainers

  root 'trainers#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
